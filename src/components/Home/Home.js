import React, {Component} from 'react';
import Header from "../Header/Header";
import Navigation from "../Navigation/Navigation";
import Contact from "../Contact/Contact";

class Home extends Component {

    state = {
      showContact: false
    };

    toggleContact = () => {
        this.setState({showContact: !this.state.showContact})
    };

    render() {
        return (
            <div>
                <Header/>
                <Navigation toggleContact={this.toggleContact} />
                {this.state.showContact ? <Contact/> : null}
            </div>
        );
    }
}

export default Home;