import React from 'react';
import {Link} from 'react-router-dom';
import './Navigation.scss';

const Navigation = (props) => {



    return (
        <nav className={'navigation'}>
            <Link className="navigation-item" to={'/projects'} >Projects</Link>
            <button className="navigation-item" onClick={props.toggleContact}>Contact</button>
            <button className="navigation-item">View/Download Resume</button>
        </nav>
    );
};

export default Navigation;