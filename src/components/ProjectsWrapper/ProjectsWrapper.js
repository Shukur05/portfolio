import React from 'react';
import Project from "../Project/Project";
import './ProjectWrapper.scss';
import {Link} from "react-router-dom";

const ProjectsWrapper = (props) => {
    return (
       props.projectData ?  <div className={'projects-container'}>
            <div className="header">
                <h1 className={'project-title'}>Projects</h1>
                <Link to={'/'} className="exit-button">X</Link>
            </div>
            <div className="projects">
                {props.projectData.map( project => {
                    return <Project project={project}/>
                })}
            </div>
        </div> : null
    );
};

export default ProjectsWrapper;