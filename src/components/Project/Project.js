import React from 'react';
import './Project.scss';

const Project = props => {

    const {name, desc, liveUrl, gitHubUrl} = props.project;

    return (
        <div className="projects-item">
            <img src="" alt="projects img" className={"project-image"}/>
            <div className="project-desc">
                <h2 className="project-name">{name}</h2>
                <p className="project-desc-text">{desc}</p>
                <a href={liveUrl} className="project-link">Live</a>
                <a href={gitHubUrl} className="project-link">Github</a>
            </div>
        </div>
    );
};

export default Project;