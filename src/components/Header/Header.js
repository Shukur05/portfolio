import React from 'react';
import './Header.scss';

const Header = () => {
    return (
        <div className={'header'}>
            <h1 className={'header-title'}>Hi, I'm <span className="shukur">SHUKUR</span></h1>
            <p className="header-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis debitis dolor
                earum est exercitationem pariatur quisquam quos recusandae reiciendis similique suscipit unde vero,
                voluptatum. Ab assumenda aut distinctio fugit iusto!</p>
        </div>
    );
};

export default Header;