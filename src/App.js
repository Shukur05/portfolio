import React, {useState, useEffect} from 'react';
import {Route} from 'react-router-dom';
import './Reset.scss';
import './App.scss';
import Home from "./components/Home/Home";
import ProjectsWrapper from "./components/ProjectsWrapper/ProjectsWrapper";

function App() {

    const [projectsData, setProjectsData] = useState();

    useEffect(() => {
        fetch('projects.json')
            .then(res => res.json())
            .then(result => {
                const data = [...result];
               setProjectsData(data);
            });
    }, []);

    const loadHome = () => {
        return <Home/>;
    };

    const loadProjects = () => {
        return <ProjectsWrapper projectData={projectsData}/>;
    };

    return (
        <div className="App">

            <Route exact path={'/'} render={loadHome}/>
            <Route path={'/projects'} render={loadProjects}/>

        </div>
    );
}

export default App;
